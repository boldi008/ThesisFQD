import os

import torch
import torchvision
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader, Dataset
from sklearn.model_selection import GroupShuffleSplit

from util import load_agrinorm_metadata, DAMAGES_OF_INTEREST, DATA_DIR, load_and_preprocess_image

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

def load_train_val_datasets(damage: str, undersample: bool = True, seed: int = 42, val_fraction:float = 0.2):
    df = load_agrinorm_metadata()
    target_col_name = f"question_id_{damage}"
    n_positives = df[target_col_name].sum()

    if undersample:
        # Undersample the negative class to balance the dataset
        positive_indices = np.argwhere(df[target_col_name] == True).flatten()
        negative_indices = np.argwhere(df[target_col_name] == False).flatten()
        # set numpy seed
        np.random.seed(seed)
        # Shuffle the indices randomly
        np.random.shuffle(negative_indices)
        # Take the first n_positives indices
        negative_indices = negative_indices[:n_positives]
        # Concatenate the positive indices with the negative indices
        indices = np.concatenate((positive_indices, negative_indices))
        # sort indices
        indices = np.sort(indices)
        df = df.iloc[indices]

    X = df["image_path"].values
    y = df[target_col_name].values
    groups = df["inspection_day"].values
    gss = GroupShuffleSplit(n_splits=1, test_size=val_fraction, random_state=seed)
    train_indices, val_indices = next(gss.split(X, y, groups))

    train_df = df.iloc[train_indices].reset_index(drop=True)
    val_df = df.iloc[val_indices].reset_index(drop=True)

    train_dataset = AgrinormDataset(train_df, damage, augment=True, random_crop=True)
    val_dataset = AgrinormDataset(val_df, damage, augment=False, random_crop=False)

    return train_dataset, val_dataset


class AgrinormDataset(Dataset):
    def __init__(self, df: pd.DataFrame, damage: str, data_dir: str = DATA_DIR, img_size: int = 224,
                 random_crop: bool = True, augment: bool = False
                 ):
        self.df = df
        self.target_col_name = f"question_id_{damage}"
        self.data_dir = data_dir
        self.damage = damage
        self.img_size = img_size
        self.random_crop = random_crop
        self.augment = augment

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        full_image_path = os.path.join(DATA_DIR, self.df.loc[idx, "image_path"])
        img = load_and_preprocess_image(full_image_path, img_size=self.img_size, normalize=False,
                                        random_crop=self.random_crop, augment=self.augment)
        label = int(self.df.loc[idx, self.target_col_name])

        return img, label

if __name__ == '__main__':
    #train_ds, val_ds = load_train_val_datasets("white_shoulders", undersample=True)
    #img = train_ds[0][0]
    #import matplotlib.pyplot as plt
    #plt.imshow(img.permute(1, 2, 0))
    #plt.show()

    """sanity checking image loading"""
    from PIL import Image, UnidentifiedImageError
    from tqdm import tqdm
    df = load_agrinorm_metadata()
    unloadable_images = []
    for img in tqdm(df["image_path"], desc="Testing image loading images"):
        try:
            Image.open(os.path.join(DATA_DIR, img))
        except UnidentifiedImageError:
            print(f"Could not load image {img}")
            unloadable_images.append(img)


if __name__ == '__main__':
    load_train_val_datasets("mildew", undersample=True)