import argparse

import torch
import torchvision
import torchmetrics
from torch.utils.data import DataLoader
import torchvision.transforms as T
import pytorch_lightning as pl
from pytorch_lightning.loggers import WandbLogger, CSVLogger
from pytorch_lightning.callbacks import LearningRateMonitor

from datasets import load_train_val_datasets


def load_pretrained_model(model_name: str, num_classes: int = 2, freeze_feature_extractor: bool = True):
    if model_name == "resnet50":
        model = torchvision.models.resnet50(weights=torchvision.models.ResNet50_Weights.DEFAULT)
        model.fc = torch.nn.Linear(model.fc.in_features, num_classes)

    elif model_name == "resnet152":
        model = torchvision.models.resnet152(weights=torchvision.models.ResNet152_Weights.DEFAULT)
        model.fc = torch.nn.Linear(model.fc.in_features, num_classes)

    elif model_name == "densenet121":
        model = torchvision.models.densenet121(weights=torchvision.models.DenseNet121_Weights.DEFAULT)
        model.classifier = torch.nn.Linear(model.classifier.in_features, num_classes)

    elif model_name == "vgg-16":
        model = torchvision.models.vgg16(weights=torchvision.models.VGG16_Weights.IMAGENET1K_V1)
        model.classifier[6] = torch.nn.Linear(model.classifier[6].in_features, num_classes)

    elif model_name == "mobilenet_v2":
        model = torchvision.models.mobilenet_v2(pretrained=True)
        model.classifier[1] = torch.nn.Linear(model.classifier[1].in_features, num_classes)

    else:
        raise ValueError(f"Unknown model name: {model_name}")

    for param in model.parameters():
        param.requires_grad = False if freeze_feature_extractor else True
    if hasattr(model, "classifier"):
        for param in model.classifier.parameters():
            param.requires_grad = True
    elif hasattr(model, "fc"):
        for param in model.fc.parameters():
            param.requires_grad = True
    else:
        raise ValueError("Unknown model architecture")

    return model


class LitClassifier(pl.LightningModule):

    def __init__(self, model_name: str, freeze: bool):
        super().__init__()

        self.model = load_pretrained_model(model_name, freeze_feature_extractor=freeze)

    def forward(self, x):
        x = T.Normalize(mean=[0.485, 0.456, 0.406],
                        std=[0.229, 0.224, 0.225])(x)
        return torch.sigmoid(self.model(x))

    def training_step(self, batch, batch_idx):
        x, y = batch
        pred = self.forward(x)
        y = y.float().reshape(-1, 1).repeat(1, 2)

        loss = torch.nn.functional.binary_cross_entropy(pred, y)
        accuracy = torchmetrics.functional.accuracy(pred, y, task="binary")

        self.log("train_loss", loss, prog_bar=True)
        self.log("train_accuracy", accuracy, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        pred = self.forward(x)
        y = y.float().reshape(-1, 1).repeat(1, 2)

        loss = torch.nn.functional.binary_cross_entropy(pred, y)
        accuracy = torchmetrics.functional.accuracy(pred, y, task="binary")

        self.log("val_loss", loss, sync_dist=True)
        self.log("val_accuracy", accuracy, sync_dist=True)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.model.parameters(), lr=args.lr, betas=(0.9, 0.999))
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, args.epochs)
        return {"optimizer": optimizer, "lr_scheduler": scheduler}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", type=str, default="resnet50")
    parser.add_argument("--freeze", action="store_true")
    parser.add_argument("--damage", type=str, default="mildew")
    parser.add_argument("--batch_size", type=int, default=8)
    parser.add_argument("--num_workers", type=int, default=4)
    parser.add_argument("--epochs", type=int, default=50)
    parser.add_argument("--lr", type=float, default=0.001)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--use_wandb", action="store_true")
    args = parser.parse_args()

    pl.seed_everything(args.seed)

    train_ds, val_ds = load_train_val_datasets(args.damage, seed=args.seed)

    model = LitClassifier(args.model, freeze=True)

    train_dataloader = DataLoader(train_ds, batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers)
    val_dataloader = DataLoader(val_ds, batch_size=args.batch_size, shuffle=False, num_workers=args.num_workers)

    log_name = args.model + ("_clfonly" if args.freeze else "_fullmodel") + f"_seed{args.seed}"
    if args.use_wandb:
        logger = WandbLogger(project=,
                             entity=,
                             group="improve_" + args.damage,
                             name=log_name,
                             mode = "online" if not args.debug else "offline",
                             )
    else:
        logger = CSVLogger(f"logs/{args.damage}", name=log_name)

    logger.log_hyperparams(vars(args))

    callbacks = [LearningRateMonitor(logging_interval='epoch')]
    trainer = pl.Trainer(
        max_epochs=args.epochs,
        accelerator="cuda" if torch.cuda.is_available() else "cpu",
        detect_anomaly=False,
        callbacks=callbacks,
        logger=logger,
    )
    trainer.fit(model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)
