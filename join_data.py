import pandas as pd
import wandb
from tqdm import tqdm
from collections import OrderedDict

df = pd.read_csv()
df["seed"] = list(range(0, 5)) * 30
df.to_csv(, index=False)

run_data = OrderedDict()
run_data.update({'defect': []})
run_data.update({'model': []})
run_data.update({'setup': []})
run_data.update({'val_acc': []})
run_data.update({'train_acc': []})
run_data.update({'seed': []})

api = wandb.Api()
runs = api.runs()
for run in tqdm(runs):
    if run.state == "finished":
        try:
            df = pd.DataFrame(run.scan_history())
            df["train_accuracy"] = df["train_accuracy"].ffill().bfill()
            best_ts = df.loc[df["val_accuracy"].idxmax()]
            val_acc = best_ts["val_accuracy"]
            train_acc = best_ts["train_accuracy"]
            model, setup, seed = run.name.rsplit("_", 2)
            seed = int(seed[4:])
            defect = run.group

            run_data['defect'].append(defect)
            run_data['model'].append(model)
            run_data['setup'].append(setup)
            run_data['val_acc'].append(val_acc)
            run_data['train_acc'].append(train_acc)
            run_data['seed'].append(seed)
        except:
            print("Couldn't load run: ", run.name)

pd.DataFrame(run_data).to_csv(", index=False)
