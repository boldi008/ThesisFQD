import os
import argparse

import numpy as np
import pandas as pd
from tqdm import tqdm
from PIL import Image
import torch
import torchvision.transforms as T
from sklearn.model_selection import GroupShuffleSplit
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, accuracy_score

from models.ibot import load_model as load_ibot
from util import load_agrinorm_metadata, DAMAGES_OF_INTEREST, ADDITIONAL_DAMAGES, DATA_DIR, load_and_preprocess_image

BATCH_SIZE = 16  # used for DINO inference



DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def load_and_preprocess_subset(X, y):
    """TODO workaround function because some images are weirdly not loadable"""
    imgs = []
    labels = []
    for path, label in tqdm(zip(X, y), total=len(X), desc="Loading images"):
        try:
            imgs.append(load_and_preprocess_image(f"{DATA_DIR}/{path}", img_size=args.img_res))
            labels.append(label)
        except:
            pass
    return torch.stack(imgs), torch.tensor(labels)


@torch.no_grad()
def get_embeddings(model: torch.nn.Module, data: torch.FloatTensor):
    n_iter = (len(data) // BATCH_SIZE) + 1

    all_embeddings = []
    for i in tqdm(range(n_iter), desc="Calculating embeddings"):
        imgs = data[i * BATCH_SIZE:(i + 1) * BATCH_SIZE]
        embedding = model(imgs.to(DEVICE)).detach().to("cpu")
        all_embeddings.append(embedding)

    return torch.cat(all_embeddings)


def undersample_negatives(X: np.array, y: np.array):
    """
    Undersample the negative class to balance the dataset.
    """
    n_positives = sum(y)
    positive_indices = np.argwhere(y == True).flatten()
    negative_indices = np.argwhere(y == False).flatten()
    # Shuffle the indices randomly
    np.random.shuffle(negative_indices)
    # Take the first n_positives indices
    negative_indices = negative_indices[:n_positives]
    # Concatenate the positive indices with the negative indices
    indices = np.concatenate((positive_indices, negative_indices))
    # Shuffle the indices again
    np.random.shuffle(indices)
    # Return the subset of X and y
    return X[indices], y[indices]


def load_model(model_name: str):
    method, backbone = model_name.split("_")
    if method == "dinov2":
        model = torch.hub.load('facebookresearch/dinov2', f"dinov2_{backbone}")
    elif method == "ibot":
        model = load_ibot(backbone)
    else:
        raise ValueError(f"Unknown model {model_name}")
    return model


def fit_and_evaluate_model(y_col: str, img_resolution: int = 224, model_name: str = "dino_vitb14",
                           logfile: str = None,  print_full_report: bool = False, seed: int =42,
                           save_probas=False):
    #assert y_col in DAMAGES_OF_INTEREST, f"y_col must be one of {DAMAGES_OF_INTEREST}"
    assert img_resolution // 14 == img_resolution / 14, "img_resolution must be divisible by 14"

    torch.manual_seed(seed)
    np.random.seed(seed)

    classifiers = {
        "LogisticRegression": (LogisticRegression(random_state=seed), {"C": [0.1, 1, 10]}),
        "kNN": (KNeighborsClassifier(), {"n_neighbors": [1, 3, 5]}),
        "SVM": (SVC(random_state=seed, probability=True), {"gamma": [1e-3, 1e-4], "C": [1, 10, 100, 1000]})
    }

    model = load_model(model_name).to(DEVICE)
    model.eval()

    df = load_agrinorm_metadata()

    X = df["image_path"].values
    y = df[f"question_id_{y_col}"].values  # change to single defect of interest
    groups = df["inspection_day"].values

    gss = GroupShuffleSplit(n_splits=1, test_size=0.2, random_state=seed)
    train_indices, test_indices = next(gss.split(X, y, groups))

    X_train, y_train = X[train_indices], y[train_indices]
    X_test, y_test = X[test_indices], y[test_indices]

    # we exepct to have way more negatives than positives, therefore we undersample to balance the dataset
    X_train, y_train = undersample_negatives(X_train, y_train)
    X_test, y_test = undersample_negatives(X_test, y_test)

    test_filenames = X_test.copy()

    # load images from disk
    X_train, y_train = load_and_preprocess_subset(X_train, y_train)
    X_test, y_test = load_and_preprocess_subset(X_test, y_test)

    # create embeddings
    X_train = get_embeddings(model, X_train)
    X_test = get_embeddings(model, X_test)

    for classifier_name, (clf, param_grid) in classifiers.items():

        gs = GridSearchCV(estimator=clf, param_grid=param_grid, cv=5, n_jobs=6, verbose=0)

        gs.fit(X_train, y_train)

        # Use the best model to make predictions on the test set
        best_model = gs.best_estimator_

        y_pred_train = best_model.predict(X_train)
        y_pred_test = best_model.predict(X_test)

        if save_probas:
            print(f"Saving probas for classifier {classifier_name}")
            y_probas_test = best_model.predict_proba(X_test)
            proba_df = pd.DataFrame()
            proba_df["filename"] = test_filenames
            proba_df["y_true"] = y_test.to(int)
            proba_df["y_proba_0"] = y_probas_test[:, 0]
            proba_df["y_proba_1"] = y_probas_test[:, 1]
            proba_df.to_csv(f"results/probas_{y_col}_{model_name}_{classifier_name}_seed_{seed}.csv", index=False)


        if print_full_report:
            print(classification_report(y_train, y_pred_train))
            print(classification_report(y_test, y_pred_test))

        train_acc = accuracy_score(y_train, y_pred_train)
        test_acc = accuracy_score(y_test, y_pred_test)

        print(f"Accuracies: Train: {train_acc}, Test: {test_acc}")
        print("==" * 10)

        with open(logfile, "a") as f:
            f.write(f"{y_col}, {img_resolution}, {model_name}, {classifier_name}, {train_acc}, {test_acc}, {seed}\n")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--model", type=str, default="dinov2_vitb14")
    parser.add_argument("--img_res", type=int, default=224)
    parser.add_argument("--damage", type=str, default="white_shoulders")
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--save_probas", action="store_true")
    args = parser.parse_args()

    os.makedirs("results", exist_ok=True)
    logfile = "results/results_ssl+shallow.csv"
    if not os.path.isfile(logfile):
        with open(logfile, "w") as f:
            f.write("damage, img_resolution, model, classifier, train_acc, test_acc, seed\n")

    fit_and_evaluate_model(args.damage, img_resolution=args.img_res, model_name=args.model, logfile=logfile,
                           seed=args.seed, save_probas=args.save_probas)
