import os

import pandas as pd
import matplotlib.pyplot as plt

from util import DATA_DIR, load_and_preprocess_image

V1 = ['white_shoulders', 'mildew', 'mechanical_damage', 'ripeness', 'green_tips']
V1_REST = ['mechanical_damage', 'ripeness', 'green_tips']
V2 = ['brown_calyx', 'cracked_heads', 'decay_mould', 'dry_bruising', 'misshaped', 'firmness', 'insects_damage', 'start_decay_mould', 'wet_bruising']
ALL = V1 + V2

for damage in ALL:
    try:
        df = pd.read_csv(f)
    except:
        continue

    top_false_neg = df[(df["y_true"] == 1) & (df["y_proba_1"] < .5)].sort_values(by="y_proba_1", ascending=True).head(16)[["filename", "y_proba_1"]].values
    top_false_pos = df[(df["y_true"] == 0) & (df["y_proba_1"] >= .5)].sort_values(by="y_proba_1", ascending=False).head(16)[["filename", "y_proba_1"]].values

    for desc, files in [("False Negatives", top_false_neg), ("False Positives", top_false_pos)]:

        fig, ax = plt.subplots(4, 4, figsize=(10,10))
        ax = ax.ravel()
        [_ax.axis("off") for _ax in ax]

        for i, (img_path, proba) in enumerate(files):
            img = load_and_preprocess_image(DATA_DIR + "/" + img_path, img_size=244, normalize=False, random_crop=False, augment=False)
            ax[i].set_title(f"p={proba:.2f}")
            ax[i].imshow(img.permute(1,2,0))
        fig.suptitle(f"Top 16 {desc} for {damage}")
        plt.tight_layout()
        os.makedirs(f"results/qualitative_analysis", exist_ok=True)
        plt.savefig(f"results/qualitative_analysis/{damage}_{'fn' if desc == 'False Negatives' else 'fp'}.png")
        plt.show()
